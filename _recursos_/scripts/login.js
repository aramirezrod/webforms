var configuracion = {
	"SENASICA-1-024-C" : { service : ofspLogin},
	"SENASICA-1-031-B" : { service : senasicaLogin}
};

var codigosMensaje = {
	"error.campo.longitud.maxima" : "La longitud máxima del campo debe ser de 12 caracteres",
	"error.credenciales.erroneas" : "Las credenciales indicadas no son validas"
};

var tramite = null;
    	
 /**
 *	Callback invocado al terminar de cargar la estructura DOM de la página
 */
function init() {
	tramite = getQueryParam( "tramite")
	
	if ( !configuracion[ tramite]) {
		showFormError( "Identificador de tramite no indicado en la petición o no definido: " + (tramite || "") );
		$("#loginForm").hide(); 
	} else {
		$( "#buttonRegistrar").show();
		$( "#buttonSubmit").show();
	}
	
}



function ofspLogin( username, password) {
	jQuery.post( "loginAjax", { 
        username    : username,
        password    : password
    }, function callbackInvocacion( respuesta, status, xHR) {
    	console.info( respuesta);
    	
    	if ( respuesta.codigo != 200) {
    		mostrarErrores( respuesta); 
    	
    	} else {
    		window.location.href = "modulos/solicitudes/registros.html?tramite=" + tramite;
    	}
    	
    });
}

function senasicaLogin( username, password) {
	showFormError( "Login SENASICA no disponible");
}

function onClick_RegistrarUsuario( event) {
	window.location.href = "registro.html?tramite=" + tramite;
}

function onClick_Entrar( event) {
	limpiarErrores();
	
    if ( !validarCampos()) {
    	return;
    }

    var username = $( "#username").val();
    var password = $( "#password").val();
    
    configuracion[ tramite].service( username, password);
}



function validarCampos() {
    var camposValidos = true;
    
	var campo = "username";
	var valor = $( "#" + campo).val();

	if ( !valor) {
		 showFieldError( campo, "Este campo es requerido");
		 camposValidos = false;
	}
	
	campo = "password";
	valor = $( "#" + campo).val();

	if ( !valor) {
		 showFieldError( campo, "Este campo es requerido");
		 camposValidos = false;
	}
	
	return camposValidos;
}

function mostrarErrores( respuesta) {
	var items = respuesta.mensajes;
	var longitud = items.length;
	var item = null;
	
	for ( var i = 0;  i < longitud; ++i) {
		item = items[ i];
		
		if( item.campo) {
			mostrarMensajeCampo( item);
		} else {
			mostrarMensajeGeneral( item)
		}
		
	}
} 


function limpiarErrores() {
	hideFormError();
	hideFieldError( "username");
	hideFieldError( "password");
}

function mostrarMensajeGeneral( mensajeItem) {
	var tipo  = mensajeItem.tipo;
	var texto = mensajeItem.mensaje;
	
	if ( tipo == "KEY") {
		texto = codigosMensaje[ texto];
	}
	
	showFormError( texto);
}

function showFormError( mensaje) {
	$( "#formErrorMessage").text( mensaje);
	$( "#formError").show();
}

function hideFormError() {
	$( "#formErrorMessage").text( "");
	$( "#formError").hide();        	
}

function mostrarMensajeCampo( mensajeItem) {
	var campo = mensajeItem.campo;
	var tipo  = mensajeItem.tipo;
	var texto = mensajeItem.mensaje;
	
	if ( tipo == "KEY") {
		texto = codigosMensaje[ texto];
	}
	
	showFieldError( campo, texto);
}


function showFieldError( campo, mensaje) {
	$( "#" + campo + "Field").addClass( 'has-error');
 	$( "#" + campo + "Error").text( mensaje).show();
}

function hideFieldError( campo) {
	$( "#" + campo + "Field").removeClass( 'has-error');
 	$( "#" + campo + "Error").text( "").hide();
}

function getQueryParam( param) {
    location.search.substr(1).split("&").some( function(item) {
    	// returns first occurence and stops
    	return item.split("=")[0] == param && (param = item.split("=")[1])
    });
    return param
}